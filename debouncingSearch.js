import { LightningElement } from "lwc";
import searchContacts from "@salesforce/apex/SearchController.searchContacts";
export default class DebouncingSearch extends LightningElement {
    searchKeyDebounced;

    doneTypingInterval = 300;
    typingTimer;

    contacts;

    handleSeachKeyChangeDebounced(event){
        clearTimeout(this.typingTimer);

        let value = event.target.value;

        this.typingTimer = setTimeout(() => {
            if(value){
                searchContacts({ searchKey: value }).then((result) => {
                    this.contacts = result;
                })
            }
        }, this.doneTypingInterval);
    }
}